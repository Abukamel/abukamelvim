#!/bin/bash
yum -y install vim* python-pip git
pip install pyflakes
rm -rf /root/.vim*
cd /root/
mkdir -p .vim/{autoload,bundle}
cd .vim
git init
git submodule add https://github.com/andrewle/vim-autoclose.git bundle/vim-autoclose
git submodule add https://github.com/vim-scripts/Color-Sampler-Pack.git bundle/color-sampler-pack
git submodule add https://github.com/sjl/gundo.vim.git bundle/gundo
git submodule add https://github.com/sjbach/lusty.git bundle/lusty 
git submodule add https://github.com/cburroughs/pep8.git bundle/pep8
git submodule add https://github.com/vim-scripts/pydoc.vim.git bundle/pydoc
git submodule add https://github.com/tpope/vim-pathogen.git bundle/pathogen
git submodule add https://github.com/vim-scripts/ScrollColors.git bundle/scrollColors
git submodule add https://github.com/ervandew/supertab.git bundle/supertab
git submodule add https://github.com/jmcantrell/vim-virtualenv.git bundle/vim-virtualenv 
git submodule init
git submodule update
git submodule foreach git submodule init
git submodule foreach git submodule update
ln -s '../bundle/pathogen/autoload/pathogen.vim' autoload/pathogen.vim
git add autoload
cat <<EOF >/root/.vimrc
" pathogen
let g:pathogen_disabled = [ 'pathogen' ]    " don't load self 
call pathogen#infect()                      " load everyhting else
call pathogen#helptags()                    " load plugin help files

" code folding
set foldmethod=indent
set foldlevel=2
set foldnestmax=4

" indentation
set autoindent
set softtabstop=4 shiftwidth=4 expandtab

" visual
highlight Normal ctermbg=black
set background=dark
set cursorline
set t_Co=256

" syntax highlighting
syntax on
filetype on                 " enables filetype detection
filetype plugin indent on   " enables filetype specific plugins

" colorpack
colorscheme vibrantink

" gundo
nnoremap <F5> :GundoToggle<CR>

" lusty
set hidden

" pep8
let g:pep8_map='<leader>8'

" supertab
au FileType python set omnifunc=pythoncomplete#Complete
let g:SuperTabDefaultCompletionType = "context"
set completeopt=menuone,longest,preview
"
EOF
ln -s /root/.vim/.vimrc /root
git commit -m "Initial commit."
echo "export EDITOR=/usr/bin/vim" >> /root/.bashrc 
